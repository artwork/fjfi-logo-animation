# FJFI logo animation

Animated [FJFI logo](https://www.fjfi.cvut.cz/images/modules/fjfi_logo.png) using pure CSS and SVG.

The logos can be previewed here:

- [version 1](https://jlk.fjfi.cvut.cz/fjfi-logo-animation/version%201/logo.html)
- [version 2](https://jlk.fjfi.cvut.cz/fjfi-logo-animation/version%202/logo.html)

## License

The work is available under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.

The authors of this work are:

- Nikita Alikantsev
- Jakub Klinkovský

The work is inspired by the [official FJFI logo](https://www.fjfi.cvut.cz/images/modules/fjfi_logo.png),
created by unknown authors.
